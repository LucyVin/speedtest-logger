package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/lucyvin/speedtest-logger/config"
	"gitlab.com/lucyvin/speedtest-logger/exporter"
	"gitlab.com/lucyvin/speedtest-logger/result"
)

var conf *config.Config
var exp *exporter.Exporter

func speedCheck(done <-chan bool, resChan chan result.Result, errChan chan error, interval time.Duration) {
	tick := time.NewTicker(interval)

	//defer tick.Stop()

	for {
		select {
		case <-done:
			return
		case <-tick.C:
			res, err := result.New()
			if err != nil {
				errChan <- err
			}
			resChan <- res
		}
	}
}

func init() {
	cfg, err := config.New("")
	if err != nil {
		panic(err)
	}
	conf = cfg

	e, errs := exporter.Initialize(conf)
	if len(errs) > 0 {
		fmt.Println(err)
	}

	exp = &e
}

func main() {
	sigc := make(chan os.Signal, 1)
	done := make(chan bool)
	resChan := make(chan result.Result)
	errChan := make(chan error)

	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
		os.Interrupt,
	)

	if conf.Prometheus.Enabled {
		fmt.Printf("Starting up prometheus server on %s\n", conf.Prometheus.Port)
		http.Handle("/metrics", exp.Prometheus.Handler)
		go http.ListenAndServe(":"+conf.Prometheus.Port, nil)
	}

	//run once before entering goroutine
	res, err := result.New()
	if err != nil {
		fmt.Println(err)
	}

	errs := res.Export(exp.Exporters())
	if len(errs) > 0 {
		for _, e := range errs {
			fmt.Println(e)
		}
	}

	go speedCheck(done, resChan, errChan, conf.Interval())

	for {
		select {
		case <-sigc:
			done <- true
			if conf.Sqlite.Enabled {
				exp.Sqlite.Close()
			}
			return
		case r := <-resChan:
			res := &r
			errs := res.Export(exp.Exporters())
			if len(errs) > 0 {
				for _, e := range errs {
					fmt.Println(e)
				}
			}
		case e := <-errChan:
			fmt.Println(e)
		}
	}
}
