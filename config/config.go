package config

import (
	"os"
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	TestInterval int           `mapstructure:"test-interval,TEST_INTERVAL"`
	IntervalType string        `mapstructure:"interval-type,INTERVAL_TYPE"`
	LogFile      string        `mapstructure:"logfile,LOG_FILE" default:"/var/speedtest-logger/run.log"`
	STDOut       bool          `mapstructure:"stdout,STDOUT_ENABLED"`
	LiveConfig   bool          `mapstructure:"live-config,LIVE_CONFIG"`
	Prometheus   *PromConfig   `mapstructure:"prometheus"`
	Sqlite       *SqliteConfig `mapstructure:"sqlite"`
	Json         *JsonConfig   `mapstructure:"json"`
}

type PromConfig struct {
	Port    string `mapstructure:"port,PROMETHEUS_PORT"`
	Enabled bool   `mapstructure:"enabled,PROMETHEUS_ENABLED"`
}

type SqliteConfig struct {
	File    string `mapstructure:"file,SQLITE_FILE"`
	Enabled bool   `mapstructure:"enabled,SQLITE_ENABLED"`
}

type JsonConfig struct {
	File    string `mapstructure:"file,JSON_FILE"`
	Enabled bool   `mapstructure:"enabled,JSON_ENABLED"`
}

//look first in home dir's .config, then pwd, then /etc/

func New(configFile string) (*Config, error) {
	conf := &Config{}
	homeDirConfigPath, _ := os.UserHomeDir()
	var defaultConfigLocations [3]string = [3]string{homeDirConfigPath + "/.config/speedtest-logger/", ".", "/etc/speedtest-logger/"}
	for _, p := range defaultConfigLocations {
		viper.AddConfigPath(p)
	}

	if configFile != "" {
		viper.AddConfigPath(configFile)
	}

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	// set some defaults

	// these three will be overwritten by some logic later depending on additional logic
	viper.SetDefault("prometheus.enabled", true)
	viper.SetDefault("sqlite.enabled", true)
	viper.SetDefault("json.enabled", true)

	// these should always be set, so let's ensure there's defaults
	viper.SetDefault("live-config", false)
	viper.SetDefault("test-interval", 10)
	viper.SetDefault("interval-type", "m")

	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return &Config{}, err
	}

	err = viper.Unmarshal(&conf)
	if err != nil {
		return &Config{}, err
	}

	if conf.Json.File == "" {
		conf.Json.Enabled = false
	}

	if conf.Sqlite.File == "" {
		conf.Sqlite.Enabled = false
	}

	if conf.Prometheus.Port == "" {
		conf.Prometheus.Enabled = false
	}

	return conf, nil
}

func (c *Config) Interval() time.Duration {
	dur := time.Duration(c.TestInterval)
	if c.IntervalType == "" && c.TestInterval == 0 {
		return time.Duration(60) * time.Second
	}

	switch c.IntervalType {
	case "m", "M":
		return dur * time.Minute
	case "h", "H":
		return dur * time.Hour
	default:
		return dur * time.Second
	}
}
