package exporter

import (
	"encoding/json"
	"fmt"

	"gitlab.com/lucyvin/speedtest-logger/result"
)

type StdoutExporter struct {
}

func (s *StdoutExporter) Export(r *result.Result) error {
	data, err := json.Marshal(r)
	if err != nil {
		return err
	}
	fmt.Println(string(data))
	return nil
}
