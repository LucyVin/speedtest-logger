package exporter

import (
	"gitlab.com/lucyvin/speedtest-logger/config"
	"gitlab.com/lucyvin/speedtest-logger/result"
)

type Exporter struct {
	Sqlite     *SqliteExporter
	Prometheus *PrometheusExporter
	Json       *JsonExporter
	STDOut     *StdoutExporter
	exporters  []result.Exporter
}

func Initialize(c *config.Config) (Exporter, []error) {
	errs := make([]error, 0)
	exporters := make([]result.Exporter, 0)
	ex := Exporter{}
	if c.Json.Enabled {
		je := &JsonExporter{c.Json.File}
		exporters = append(exporters, je)
		ex.Json = je
	}
	if c.Sqlite.Enabled {
		sqe := &SqliteExporter{Database: c.Sqlite.File}
		db, err := sqe.Init()
		if err != nil {
			errs = append(errs, err)
		} else {
			exporters = append(exporters, &db)
			ex.Sqlite = &db
		}
	}
	if c.Prometheus.Enabled {
		pe := PrometheusExporter{}
		pe = pe.Init()

		exporters = append(exporters, &pe)
		ex.Prometheus = &pe
	}
	if c.STDOut {
		exporters = append(exporters, &StdoutExporter{})
		ex.STDOut = &StdoutExporter{}
	}

	ex.exporters = exporters
	return ex, errs
}

func (e *Exporter) Exporters() []result.Exporter {
	return e.exporters
}
