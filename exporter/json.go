package exporter

import (
	"encoding/json"
	"os"

	"gitlab.com/lucyvin/speedtest-logger/result"
)

type JsonExporter struct {
	FilePath string
}

func (j *JsonExporter) Export(r *result.Result) error {
	out, err := json.Marshal(r)
	if err != nil {
		return err
	}
	f, err := os.OpenFile(j.FilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(string(out) + "\n")
	if err != nil {
		return err
	}

	return nil
}
