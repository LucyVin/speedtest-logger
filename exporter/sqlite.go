package exporter

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/lucyvin/speedtest-logger/result"
)

type SqliteExporter struct {
	Database string
	*sql.DB
}

func (s SqliteExporter) Init() (SqliteExporter, error) {
	db, err := sql.Open("sqlite3", s.Database)
	if err != nil {
		return SqliteExporter{}, err
	}
	return SqliteExporter{s.Database, db}, nil
}

func (s *SqliteExporter) Export(r *result.Result) error {
	fields, vals := r.Unzip()

	// check if results table exists first
	_, exists := s.Exec("SELECT * FROM results LIMIT 1;")
	if exists != nil {
		fieldsAsCreate := "("
		for i, v := range fields {
			fieldsAsCreate = fieldsAsCreate + v + " text"
			if v == "timestamp" {
				fieldsAsCreate = fieldsAsCreate + "primary key"
			}

			if i < len(fields)-1 {
				fieldsAsCreate = fieldsAsCreate + ", "
			} else {
				fieldsAsCreate = fieldsAsCreate + ")"
			}
		}
		_, err := s.Exec(fmt.Sprintf("CREATE TABLE results %s;", fieldsAsCreate))
		if err != nil {
			return fmt.Errorf("error creating table: %s", err)
		}
	}

	valsPlaceholder := ""
	for i := 0; i < len(vals); i++ {
		valsPlaceholder = valsPlaceholder + "?"
		if i < len(vals)-1 {
			valsPlaceholder = valsPlaceholder + ", "
		}
	}
	insert := fmt.Sprintf("INSERT INTO results (%s) VALUES(%s)", strings.Join(fields, ", "), valsPlaceholder)
	tx, err := s.Begin()
	if err != nil {
		return fmt.Errorf("error creating insert statement: %s, %s", insert, err)
	}
	stmt, err := tx.Prepare(insert)
	if err != nil {
		return fmt.Errorf("error preparing statement: %s, %s", insert, err)
	}

	insVals := make([]interface{}, 0)
	for _, v := range vals {
		insVals = append(insVals, v)
	}

	defer stmt.Close()
	_, err = stmt.Exec(insVals...)
	if err != nil {
		return err
	}

	tx.Commit()
	return nil
}
