package exporter

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/lucyvin/speedtest-logger/result"
)

type PrometheusExporter struct {
	DLSpeed prometheus.Summary
	ULSpeed prometheus.Summary
	Latency prometheus.Summary
	Handler http.Handler
}

func (p PrometheusExporter) Init() PrometheusExporter {
	o := map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.95: 0.005, 0.99: 0.001}

	pe := PrometheusExporter{
		DLSpeed: promauto.NewSummary(prometheus.SummaryOpts{
			Name:       "download_speed",
			Namespace:  "speedtest_logger",
			Help:       "download speed in Mb/s",
			Objectives: o,
		}),
		ULSpeed: promauto.NewSummary(prometheus.SummaryOpts{
			Name:       "upload_speed",
			Namespace:  "speedtest_logger",
			Help:       "upload speed in Mb/s",
			Objectives: o,
		}),
		Latency: promauto.NewSummary(prometheus.SummaryOpts{
			Name:       "latency",
			Namespace:  "speedtest_logger",
			Help:       "latency in milliseconds",
			Objectives: o,
		}),
		Handler: promhttp.Handler(),
	}

	return pe
}

func (p *PrometheusExporter) Export(r *result.Result) error {
	p.DLSpeed.Observe(r.DLSpeed)
	p.ULSpeed.Observe(r.ULSpeed)
	p.Latency.Observe(float64(r.Latency.Milliseconds()))

	return nil
}
