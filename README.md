# speedtest-logger

Just keeping the ISP honest... by running a speedtest on a set interval.
Supports data exporting to sqlite, prometheus metrics, and json files as well as json in STDOUT. See config below.

built on top of [speedtest-go by Showwin](https://github.com/showwin/speedtest-go)

# Requirements
- Sqlite3
- CGO enabled
- gcc

# Installation and Usage
1. Download the repository: `git clone https://gitlab.com/LucyVin/speedtest-logger.git`
2. CD into the repo and build the binary file: `cd speedtest-logger && go build -o speedtest-logger main.go`
3. Put the binary somewhere in your path, eg `/usr/local/bin/speedtest-logger`
4. Create a config file at `$HOME/.config/speedtest-logger/config.yml`OR `/etc/speedtest-logger/config.yml`
5. Run for as long as you want

# Running with Systemd
1. Copy and modify the following unit file, to `/etc/systemd/system/speedtest-logger.service`
2. Run `sudo systemctl enable speedtest-logger.service && sudo systemctl start speedtest-logger.service`
```
[Unit]
Description=Speedtest Logger

[Service]
ExecStart=/usr/local/bin/speedtest-logger # or wherever you placed the executable
User=USER # replace this if you want to run as a user, delete to run with default user settings
Group=USER # replace this if you want to run with a specific group, delete to run with default
Restart=always

[Install]
WantedBy=multi-user.target
```
# Config file structure

```yml
# frequency of test runs
test-interval: 30
# define the kind of interval (hour: h, second: s, minute: m)
interval-type: s
# not yet implemented
log-file: foo.log
# mostly for debug purposes, but for now use to redirect logging
stdout: true
# not yet enabled
live-config: false
# we have prom configured but not enabled below
prometheus:
  port: 1121
  enabled: false
# sqlite configured and enabled
sqlite:
  file: test.db
  enabled: true
# json implicitly enabled due to file being conf'd, disable with `enabled: false` or removing block
json:
  file: test.log

```
