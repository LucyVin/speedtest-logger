package result

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/showwin/speedtest-go/speedtest"
)

type Exporter interface {
	Export(r *Result) error
}

type Result struct {
	Host      string        `json:"host"`
	Location  string        `json:"location"`
	Distance  float64       `json:"distance"`
	Latency   time.Duration `json:"latency"`
	ULSpeed   float64       `json:"upload_speed"`
	DLSpeed   float64       `json:"download_speed"`
	Sponsor   string        `json:"sponsor"`
	URL       string        `json:"url"`
	URL2      string        `json:"url2"`
	IP        string        `json:"ip"`
	Country   string        `json:"country"`
	Timestamp time.Time     `json:"timestamp"`
	ISP       string        `json:"isp"`
	stringMap map[string]interface{}
}

func New() (Result, error) {
	user, err := speedtest.FetchUserInfo()
	if err != nil {
		return Result{}, err
	}
	sl, err := speedtest.FetchServers(user)
	if err != nil {
		return Result{}, err
	}
	s, err := sl.FindServer([]int{})
	if err != nil {
		return Result{}, err
	}

	test := s[0]
	test.PingTest()
	err = test.DownloadTest(false)
	if err != nil {
		return Result{}, err
	}
	err = test.UploadTest(false)
	if err != nil {
		return Result{}, err
	}

	res := Result{
		Distance:  test.Distance,
		Latency:   test.Latency,
		Host:      test.Host,
		Location:  test.Name,
		ULSpeed:   test.ULSpeed,
		DLSpeed:   test.DLSpeed,
		Sponsor:   test.Sponsor,
		URL:       test.URL,
		URL2:      test.URL2,
		IP:        user.IP,
		Country:   test.Country,
		ISP:       user.Isp,
		Timestamp: time.Now(),
	}

	proto, _ := json.Marshal(res)
	var sm map[string]interface{}

	json.Unmarshal(proto, &sm)
	res.stringMap = sm

	return res, nil
}

// returns two slices comprised of keys and values as would be represented in json
func (r *Result) Unzip() ([]string, []string) {
	keys := make([]string, 0)
	vals := make([]string, 0)

	for k, v := range r.stringMap {
		keys = append(keys, k)
		vals = append(vals, fmt.Sprintf("%v", v))
	}

	return keys, vals
}

// returns a list of struct fields as their export string value
func (r *Result) FieldStrings() []string {
	fields := make([]string, 0)

	for k := range r.stringMap {
		fields = append(fields, k)
	}

	return fields
}

// calls Export() on a collection which fulfills the Exporter interface
func (r *Result) Export(e []Exporter) []error {
	errs := make([]error, 0)
	for _, exp := range e {
		err := exp.Export(r)
		if err != nil {
			errs = append(errs, err)
			continue
		}
	}
	if len(errs) == 0 {
		return nil
	} else {
		return errs
	}
}
